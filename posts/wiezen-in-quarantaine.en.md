<!--
.. title: Quarantaine whist with WDEBELEK
.. slug: wiezen-app
.. date: 2020-07-15 16:47:00 UTC+02:00
.. tags: wiezen,app
.. category:
.. link:
.. description:
.. type: text
-->

Now that the daily number of new COVID-19 infections is increasing,
and people are talking about the possibility of local lockdowns,
chances increase that card playing evenings will be canceled because
of corona reasons.

But, as opposed to the previous lockdown, now there is an easy solution
to play solo whist remotely. Because I work on a web application for
online card playing: [WDEBELEK](https://www.rijkvanafdronk.be/apps/wdebelek).

It is still a young application, with a lot of loose ends, but it has been
tested extensively, and it seemed to be useful. How does it work:

1. Find 3 to 5 friends that want to play cards with you.
2. Start a video conference with these friends, using your favourite video
   conferencing tool.
3. Create a card table at [kaart.rijkvanafdronk.be](https://kaart.rijkvanafdronk.be).
4. Create invitation links, and pass them on to your friends.
5. Play cards!

We do have a [basic manual](https://www.rijkvanafdronk.be/apps/wdebelek), but
that's in Dutch. (If anybody wants to translate it to English: let me know.)
Anyway, the use of the application should be straight forward.

[Source code](https://gitlab.com/rva-vzw/wdebelek) is freely available under
the conditions of the AGPL v3 or later. And you can use the software for fee;
that is: as long as my server can handle the load.

Enjoy!

![kaarten in quarantaine](/galleries/misc/quarantaine.jpg)
