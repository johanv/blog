<!--
.. title: Iemand zin om mijn code te reviewen?
.. slug: code-review
.. date: 2021-03-29 20:47:00 UTC+02:00
.. tags: wdebelek,php,codereview
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /galleries/cards/codereview.jpg
-->

Laat ons het hebben over code review. Waarschijnlijk bestaan er verschillende
definities van 'code review', ik zal eerst even verduidelijken wat ik met
code review bedoel.

In grote lijnen: alvorens er aanpassingen gebeuren aan de broncode van
je software, is het de bedoeling dat één van je collega's er eens naar gekeken
heeft. Zodat die collega vragen kan stellen, of opmerkingen kan maken.

![speelkaart en broncode](/galleries/cards/codereview.jpg)

<!-- TEASER_END -->

De voordelen liggen voor de hand:

* Een extra paar ogen dat naar je code kijkt, vergroot de kans dat
  er bugs opgemerkt worden.
* Aan de hand van de vragen en opmerkingen die je krijgt, kom je
  te weten waar je code moeilijk leesbaar is, of waar je rare
  kronkels hebt gemaakt. En dus ook waar je best nog iets aanpast
  of extra documenteert. Dat gaat helpen als je na 3 maand
  plots je eigen code opnieuw moet bekijken.
* Door zelf code van je collega's te bekijken, hou je een overzicht
  op het geheel van de source code. Op den duur ken je de
  code quasi van binnen en van buiten.
* Als je weet dat er iemand naar je code kijkt, ga je automatisch
  zorgvuldiger programmeren.

Uiteraard werkt dit het beste als je met 2 of meer mensen aan een
project werkt, en als iedereen de tijd neemt om af en toe de code
van een collega te bekijken.

Nu wil het toeval dat ik een aantal (hobby-)projectjes heb waar
ik alleen aan werk. Dus ik heb geen collega om mijn code te reviewen.
Daarom vraag ik het aan jou.

In een vorige blog post, probeerde ik uit te leggen 
[hoe de architectuur van wdebelek in elkaar zit](/posts/event-sourced-wiezen).
En nu ben ik dus op zoek naar iemand die de code wil bekijken
die ik maakte voor issue
[#84](https://gitlab.com/rva-vzw/wdebelek/-/issues/84) van
WDEBELEK. [WDEBELEK](https://www.rijkvanafdronk.be/apps/wdebelek)
is een toepassing om via het internet te kaarten, slaggebaseerde
spelletjes, in de mate van het mogelijke spelregelonafhankelijk.
De bedoeling van issue 84 was dat je na een spelletje, de gehaalde
slagen van de teams opnieuw kunt bekijken. Dit is nuttig voor
spelletjes waarbij je punten moet tellen, zoals klaverjassen en
manillen.

De code die ik hiervoor schreef, zit in
[merge request !149](https://gitlab.com/rva-vzw/wdebelek/-/merge_requests/149/diffs).
Ik maakte hierin een nieuw
[read model](/posts/event-sourced-wiezen/#read_side), dat bijhoudt
welke slagen er zijn gehaald, en de slagen 'discloset' als de
laatste slag is opgeraapt. Hoe ik een read model deftig organiseer,
daar ben ik nog steeds niet zeker van, dus wat code review kan
zeker geen kwaad.

Het is nu wel zo dat het merge request al gemerged is. Het zou
beter geweest zijn als ik het eerst had laten reviewen,
wie weet had een review een
[drietal](https://gitlab.com/rva-vzw/wdebelek/-/merge_requests/150/diffs)
[bugs](https://gitlab.com/rva-vzw/wdebelek/-/merge_requests/152/diffs)
kunnen
[vermijden](https://gitlab.com/rva-vzw/wdebelek/-/merge_requests/153/diffs).

Zodus, als je iets afweet van programmeren, en je hebt zin om mijn
code kritisch te bekijken, hier is het merge request nog eens:

[gitlab.com/rva-vzw/wdebelek/-/merge_requests/149/diffs](https://gitlab.com/rva-vzw/wdebelek/-/merge_requests/149/diffs).

Als je een gitlab-account hebt, dan kun je je comments rechtstreeks
typen bij de lijnen waar je vragen of opmerkingen bij hebt. En
anders mag je ze me [op een andere manier](/pages/contact) bezorgen.
Wie weet welke bugs komen nog boven, en ik twijfel er niet aan
dat ik kan bijleren van jullie ideeën.

En als tegenprestatie wil ik misshien mijn oog ook wel eens laten vallen op
een stuk code van jou.
