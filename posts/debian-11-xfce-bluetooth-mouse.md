<!--
.. title: Debian 11, XFCE and a bluetooth mouse
.. slug: debian-11-xfce-bluetooth-mouse
.. date: 2023-02-06 21:15:00 UTC+01:00
.. tags: linux,debian
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /galleries/cards/bluetooth-mouse.jpg
-->

Last week, I gave an old laptop a second life. It's an old ASUS, I think it came
with Windows 8 back in the day, and I installed Debian 11 with XFCE on it. This looks
very retro, but it works like a charm. I can now use the battery for serveral
hours, while with Windows it would die after 5 minutes. And today I got my bluetooth
mouse working.

![a bluetooth logitech mouse, sitting on the laptop](/galleries/cards/bluetooth-mouse.jpg)

Here is what I did:

<!-- TEASER_END -->

```bash
apt install blueman
apt install bluez-firmware # not sure this was actually needed
apt install bluez-tools # update 2023-09-06: This is actually needed
```

With blueman installed, the menu under 'Applications', 'Settings' now contains
an item 'Bluetooth Manager'. The problem was that it discovered all kinds
of bluetooth devices, but not my mouse 🙃

To solve that, I also had to

```bash
modprobe uhid
```

(Thank you, [Stack Exchange](https://unix.stackexchange.com/a/722720/559630)).

So I added `uhid` to `/etc/modules`, to make it load at boot time.

**Update 2023-02-14:** After I had used the mouse on an other device, it didn't show up
anymore in the Bluetooth Manager. I got it working again by running 

```bash
bt-adapter -d
```

in a terminal.
