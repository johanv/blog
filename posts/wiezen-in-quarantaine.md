<!--
.. title: Wiezen in quarantaine met WDEBELEK
.. slug: wiezen-app
.. date: 2020-07-15 16:18:00 UTC+02:00
.. tags: wiezen,app,wdebelek
.. category:
.. link:
.. description:
.. type: text
-->

Nu het aantal dagelijkse nieuwe COVID-19-besmettingen een aantal
dagen op rij toeneemt, en er in de media al gesproken wordt over
mogelijke plaatselijke lockdowns, vergroot ook de kans op
kaartavonden die geannuleerd worden wegens coronagevaar.

Maar in tegenstelling tot de vorige lockdown, is er nu wel een
makkelijke oplossing om te wiezen vanuit uw kot
(en hiermee bedoel ik dan
[traditioneel wiezen](https://www.rijkvanafdronk.be/wiezen/), want
voor kleurenwiezen bestonden er al langer oplossingen).
Want ik werk aan een webtoepassing om online te kaarten:
[WDEBELEK](https://www.rijkvanafdronk.be/apps/wdebelek), in de
volksmond ook wel eens &lsquo;de wiezen-app&rsquo;. (En als ik dat
zo vermeld, wordt ze misschien makkelijker gevonden door Google 
en consoorten ;-))

De toepassing staat nog in zijn kinderschoenen, en er zijn nog
heel wat losse eindjes, maar ze is wel al uitgebreid getest,
en bruikbaar gebleken. Hoe gaat het in z'n werk:

1. Vind 3 tot 5 vrienden die samen met jou willen wiezen.
2. Start een video-conference met die vrienden; gebruik je favoriete
   video-conferencing tool. (Bijv. [praatbox.be](https://praatbox.be),
   maar uiteraard is eender welke tool geschikt.)
3. Maak een kaarttafel aan op [kaart.rijkvanafdronk.be](https://kaart.rijkvanafdronk.be).
4. Maak uitnodigingen voor je vrienden, en bezorg hen de links.
5. Kaarten maar!

Het is aangewezen om er 
[de handleiding](https://www.rijkvanafdronk.be/apps/wdebelek) een
keertje op na te slaan, maar in principe zou het allemaal vrij
intuitief moeten zijn.

[De broncode van de toepassing](https://gitlab.com/rva-vzw/wdebelek) is
vrij beschikbaar onder de
voorwaarden van de AGPL v3 of later. En je kunt ze gratis gebruiken.
Dat is: zo lang mijn server dat kan trekken, uiteraard.

Veel kaartplezier!

![kaarten in quarantaine](/galleries/misc/quarantaine.jpg)
