<!--
.. title: Selenium web tests on gitlab-ci for an app using Mercure
.. slug: gitlab-ci-selenium-and-mercure
.. date: 2020-05-08 22:15:00 UTC+02:00
.. tags: symfony,php,codeception,mercure,tests,ci,frontend
.. category:
.. link:
.. description:
.. type: text
-->

I recently learned
[how to use Mercure](https://symfony.com/doc/current/mercure.html)
so that the php/Symfony back-end of my
application can send out events that can be picked up by the
javascript on my front end. This is really cool technology, you should
definitely try this out if you haven't already done so.

Now if you write this kind of software, of course you also want to
write some web tests, so that you can automatically check if existing
features of your program keep working after you add new things.

![gitlab-ci, codeception and mercure](/galleries/misc/gitlab-codeception-mercure-smaller.png)

For these kind of tests, I use gitlab-ci and codeception, as I wrote
in [previous posts](/categories/ci). Today I got these test working
with a Mercure hub.

You need to use a mercure service in the job running your web tests,
which you can configure in `.gitlab-ci.yml`:

**Update 2020-12-03:** I use the 0.10 version of the mercure container,
because the acceptance test job started failing with 0.11, with these
error messages:

> Message: "Couldn't connect to server for "http://mercure:3000/.well-known/mercure"."
In HandleMessageMiddleware.php line 80:
>
>   Couldn't connect to server for "http://mercure:3000/.well-known/mercure".)

So here's the adapted `.gitlab-ci.yml`-file:

```yaml
variables:
  JWT_KEY: YourSecretKey
  DEMO: 1
  ALLOW_ANONYMOUS: 1
  HEARTBEAT_INTERVAL: 30s
  ADDR: ':3000'
  CORS_ALLOWED_ORIGINS: '*'
  PUBLISH_ALLOWED_ORIGINS: '*'

# ...

acceptance tests:
  # I would prefer to use a public php-7.3 image with mysqli, but
  # I don't think such a thing exists.
  # So let's reuse the image I created for the dikdikdik web tests.
  image: registry.gitlab.com/rva-vzw/dikdikdik.php
  services:
    - name: mariadb:10.4
      alias: db
    - name: selenium/standalone-chrome
      alias: chrome
    - name: dunglas/mercure
      alias: mercure:v0.10
  # (the rest of the job follows here)
```

The `JWT_KEY` should work with the `JWT` your backend uses
to publish events. The port number in `ADDR` as well. If you
use Symfony, as I do, the corresponding configuration of your
backend should be in the `.env` file, e.g.

```
MERCURE_PUBLISH_URL=http://mercure:3000/.well-known/mercure
MERCURE_JWT_TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZXJjdXJlIjp7InB1Ymxpc2giOltdfX0.FFSjymJCGRDWrmAmPJDoVGuYwnx5FRTjRFkkYfvLkUg
```

(Needless to say: don't use the production JWT and token in the
source code that is used by gitlab! If you're unsure how to
create a JWT, see the section &lsquo;Creating your JWT&rsquo;
of 
[this excellent blog post](https://thedevopsguide.com/real-time-notifications-with-mercure/).)

**Update 2020-12-03:** That blog post doesn't exist anymore, but 
[I found another page](https://medium.com/@stefan.poeltl/instant-realtime-notifications-with-symfony-and-mercure-e45270f7c8a5)
that covers mercure and the JWT token.

Now the problem I ran into, is similar to the
[general problem I had with web tests](/posts/gitlab-ci-codeception-and-selenium-web-tests):
the chrome container doesn't know where the mercure hub is;
the host name &lsquo;mercure&rsquo; is not resolved to an ip address.

**Update 2022-06-20:** *The solution described below will still work, but
I found a better one. You should just set the
`FF_NETWORK_PER_BUILD` variable to 1 in your `.gitlab-ci.yml` file,
as described [in this post](/posts/php-ci-demo). If you want to see how
this works for a real life project, you can check out the
[.gitlab-ci.yml](https://gitlab.com/rva-vzw/dikdikdik/-/blob/develop/.gitlab-ci.yml)
file of [dikdikdik](https://gitlab.com/rva-vzw/dikdikdik/), an app to
keep track of the scores when playing [solo whist](https://www.rijkvanafdronk.be/wiezen).*

To fix this, I figure out the IP address of the Mercure hub when I
start the job, and I replace the host name of the `MERCURE_PUBLISH_URL`
in the `.env` file by this IP-address. This works, since my frontend
retrieves the publish url from the backend by a rest-call.

This is how that looks like:

```
  script:
    - MERCURE_IP=$(getent hosts mercure | awk '{ print $1 }')
    - echo Mercure IP $MERCURE_IP
    - sed -i "s/mercure:3000/$MERCURE_IP:3000/" .env
```

You can review the complete job definition in the `.gitlab-ci.yml`
[file in the wdebelek repository](https://gitlab.com/rva-vzw/wdebelek/-/blob/develop/.gitlab-ci.yml#L129-161).
The file `acceptance/TrickCest.php` contains an
[example test involving server sent events](https://gitlab.com/rva-vzw/wdebelek/-/blob/develop/tests/acceptance/TrickCest.php#L42).
And hopefully you can see in the
[pipelines](https://gitlab.com/rva-vzw/wdebelek/pipelines) that all
tests passed without problems 🤓.
