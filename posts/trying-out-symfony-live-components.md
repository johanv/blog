<!--
.. title: Trying out Symfony Twig Components
.. slug: trying-out-symfony-live-components
.. date: 2021-10-18 22:18:00 UTC+02:00
.. tags: symfony,symfony-ux
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /galleries/cards/twig-components.png
-->

Last summer, I attended SymfonyWorld Online 2021 Summer Edition, and I was
fascinated by [Ryan Weaver](https://twitter.com/weaverryan)s
talk about [Symfony Twig Components](https://github.com/symfony/ux-twig-component).

As the README says on the [github project page](https://github.com/symfony/ux-twig-component)

> Twig components give you the power to bind an object to a template, making it easier 
> to render and re-use small template "units" - like an "alert", markup for a modal,
> or a category sidebar.

I found the presentation very promising, and I wanted to try using
twig components in combination with [stimulus](https://symfonycasts.com/screencast/stimulus)
for the frontend of [dikdikdik](https://gitlab.com/rva-vzw/dikdikdik), a hobby
webapp I maintain, to keep track of the score for the
[wiezen](https://en.wikipedia.org/wiki/Solo_whist) card game.

<!-- TEASER_END -->

Now we're a couple of months later, and I finally found time to write my first component.
I followed the instructions in the README, but what I got was a server error:
Unknwon "component" function.

![Unknown "component" function](/galleries/cards/twig-components.png)

I first tried to fix this to define the `ComponentExtension` class as service
in `serices.yaml`. I got more error messages, and I kept on adding all kind of 
dependendent services. Obviously this failed at some point.

So I spent an evening on messing around with configuration files, reading documentation
and sample code, and finally I found out what was wrong: the twig component bundle
was not added to `config/bundles.php`. I just had to add this line:

```
    Symfony\UX\TwigComponent\TwigComponentBundle::class => ['all' => true],
```

(See also
[commit c0080e43](https://gitlab.com/rva-vzw/dikdikdik/-/commit/c0080e43afb59f53132ec35573ad0d5aafbdb9dd)
in the dikdikdik git repository.)

Now I thought adding such a line should have been done automatically by 
flex, when installing the `symfony/ux-twig-component` package. So maybe something
is wrong with the flex recipe. But I'm not sure where to find that recipe.
When I run

```
composer recipes symfony/ux-twig-component
```

I get an error as well:

```text
  [ErrorException]                                     
  Trying to access array offset on value of type null 
```

This might be related.

Anyway, I am not sure if the issue I had is caused by a bug in 
symfony-ux-twig-component, or maybe there's just something wrong with the setup
of my Symfony project. But here you are, if you also happen to run into
this problem, now there's a solution that can be found by Google and alike 🙂.
If you have a better insight in what went wrong here, please let me know.

I'll keep you posted on using the twig components for my frontend. But keep in
mind that I have limited free time to work on this hobby project.

**Update:** I created [my first Symfony Live Component](/posts/first-symfony-live-component/)!
