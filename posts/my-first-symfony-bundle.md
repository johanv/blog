<!--
.. title: Krak? Boem! My first Symfony Bundle
.. slug: my-first-symfony-bundle
.. date: 2024-01-13 21:52:00 UTC+01:00
.. tags: php,eventsourcing,symfony,krakboem
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /galleries/cards/krakboem1.png
-->

Earilier this week, I published my first Symfony bundle. Should you use it? I don't think so.
But I'm proud that I unlocked this achievement 🏆🔓

A little background.

![picture of an elephant and playing cards](/galleries/cards/krakboem1.png)

Back in 2019, I started writing an 
[application that keeps track of the points when playing whist](/en/posts/wiezen-score-app). I called it
[dikdikdik](https://gitlab.com/rva-vzw/dikdikdik), a typical bad name for an open
source project. It refers to some catch phrase in Dutch, and it can be used
at [score.rijkvanafdronk.be](https://score.rijkvanafdronk.be). It has
become rather popular over time, which I find very cool.

So ok, I created this app because I wanted something to help us with the
scores of our card game. But maybe
at least as important: I wanted to create an app to fiddle around, and to learn
new things. I wanted to use Symfony and event sourcing, two technologies
that were still rather new to me. After some time, I had a fun project,
and I talked about it in a [lightning talk](https://jv3.johanv.org/phpbnl20) at
the last PhpBenelux conference (ever? 😢) in 2020.

Not long after that, in 2020, Covid broke out, and we couldn't play cards anymore because
of the restrictions. So we needed an [app to play cards online](/en/posts/wiezen-app).
Because I wanted that one to be event sourced as well, and because I didn't want to
write everything again, I extracted all relevant
classes and interfaces to a php package. [rva-vzw/krakboem](https://gitlab.com/rva-vzw/krakboem)
was born.

This package worked quite well for a couple of years. But things have changed.
Back then, I used php 7.4. Now there's php 8.3, with `final` and `readonly`. I know more
about the relationship between `Traversable` and `Generator`. I also have better
understanding about what infrastructure code exactly is. And recently I 
[learned how to use deciders](/en/posts/whist-with-a-decider), and
krakboem didn't know deciders.

So krakboem could use an update, and that's what I did. I rearranged the classes,
added `final` and `readonly`, introduced deciders. And while doing this, I also
extracted all symfony messenger and doctrine code, and moved that to a bundle.
So now I have a [krakboem-bundle](https://packagist.org/packages/rva-vzw/krakboem-bundle).
And I also tagged [version 1.0](https://packagist.org/packages/rva-vzw/krakboem)
of the base krakboem package 🎉

Not that I'm 100% happy with how krakboem is structured, but I didn't want to move
every class around. And I'm fairly sure that it will be useful again for a couple
of years without needing lots of changes.

The bundle is still experimental (version 0.1.2 at the moment); I don't really know 
yet how to properly create a bundle. But I am happy, because it already takes care of the
dependency injection for the doctrine and messenger infrastructure.

So next thing on my to-do will be: upgrade my other krakboem-based projects, so that they all use my
new bundle. I don't expect this to be a whole lot of work, but I don't have a whole
lot of time either. So we'll see how it works out.
