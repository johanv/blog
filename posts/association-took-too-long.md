<!--
.. title: Working around a Pop!_OS networking issue
.. slug: association-took-too-long
.. date: 2021-11-18 19:01:00 UTC+01:00
.. tags: pop-os,linux,tips
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /galleries/misc/tp-link.jpg
-->

**Update (2021-11-26):**

*The solution I describe in the original post, did work for a while, and then
stopped working. Then I got it working again by removing the kernel
modules iwlmvm and iwlwifi, and modprobing iwlwifi again. Worked
for a day, and stopped working again. So I guess it just sometimes
worked, and sometimes, it didn't.*

![the TP-Link TL-WR1043ND v2](/galleries/misc/tp-link.jpg)

*I think it was just the router. It's a TP-LINK TL-WR1043ND v2, and
I installed OpenWrt on it (v19.something), a couple of years ago.
Now I upgraded it to v 20.02.1, and I think - hope - that that
will finally have solved the problem. I was a little reluctant to flash a
firmware update, but it turned out to be very easy. The
[page for my device](https://openwrt.org/toh/tp-link/tl-wr1043nd)
on the openWRT wiki had a direct link to the 'upgrade firmware', which
I could easily flash using the openWRT web interface (using another
device than my laptop, of course).*

<!-- TEASER_END -->

*I did have to check a 'force update' checkbox somewhere, because
the previous firmware didn't recognize the update as suitable for
the device. So I went through a couple of anxious minutes during
the flashing; it took some time. But all turned out well!*

*You can read the original blog post below, but your really
shouldn't:*

Last tuesday, I got a new laptop, a Dell XPS 15 7590.
(Ok, to be fair, a 2nd hand laptop, to replace my previous XPS 15 7590,
which is sent back to Dell because of a broken webcam.)
This new laptop had [Pop!\_OS](https://pop.system76.com/)
installed, while the previous one was running Ubuntu.

This is the first time I use Pop!\_OS, and so I had some issues
with it. Like the default keyboard shortcuts, that seem to differ
from the default GNOME shortcuts. And like... the name!
I find Pop!\_OS a terrible name. It's very
hard to write correctly, and you need to escape the underscore
if you want to use Markdown.

But another problem I was experiencing, was the fact that the PC
didn't want to connect to one of my wireless routers.

I have two routers at home: One router is the one I got from my ISP, 
Telenet, to which I could connect without problems.
But I have another router as well, on which I installed OpenWRT some time ago. This
router was working for all devices I have, except for my 'new'
XPS. But I could connect to it with the previous XPS.

The same was true for connecting to my Android phone. *(Update: this was probably
another problem:)* When I enabled wifi tethering,
it didn't work with this new PC. So something
strange was going on. I tried installing another kernel, but that
didn't solve my problem.

In `/var/log/syslog`, I found these kinds of messages:

```
Nov 18 16:20:29 pop-os NetworkManager[1165]: <info>  [1637248829.5668] device (wlp59s0): supplicant interface state: disconnected -> associating
Nov 18 16:20:39 pop-os systemd[1]: NetworkManager-dispatcher.service: Succeeded.
Nov 18 16:20:39 pop-os NetworkManager[1165]: <warn>  [1637248839.5724] sup-iface[0x559e52893a10,wlp59s0]: connection disconnected (reason -3)
Nov 18 16:20:39 pop-os NetworkManager[1165]: <info>  [1637248839.5795] device (wlp59s0): supplicant interface state: associating -> disconnected
Nov 18 16:20:39 pop-os NetworkManager[1165]: <info>  [1637248839.5966] device (wlp59s0): supplicant interface state: disconnected -> associating
Nov 18 16:20:49 pop-os NetworkManager[1165]: <warn>  [1637248849.6043] sup-iface[0x559e52893a10,wlp59s0]: connection disconnected (reason -3)
Nov 18 16:20:49 pop-os NetworkManager[1165]: <info>  [1637248849.6094] device (wlp59s0): supplicant interface state: associating -> disconnected
Nov 18 16:20:49 pop-os NetworkManager[1165]: <info>  [1637248849.7103] device (wlp59s0): supplicant interface state: disconnected -> scanning
Nov 18 16:20:54 pop-os NetworkManager[1165]: <warn>  [1637248854.3040] device (wlp59s0): Activation: (wifi) association took too long
```

I was looking on the internet for a solution, and I found that I
could try to use [WICD](https://help.ubuntu.com/community/WICD)
instead of NetworkManager. But I didn't want to do that, because
I use NetworkManager for my OpenVPN connection for work;
I wasn't sure I could easily set up a VPN connection when
using WICD. (*Update: I don't think this is even possible with
Pop!_OS*)

Finally I found a workaround
[on askubuntu.com](https://askubuntu.com/a/1237769/380688). It seems
you have to disable NetworkManager's MAC address randomization.
You can do this using the GUI, via Settings, Wifi, and then clicking
the *settings gear* (⚙) of your network.


What I did, was clicking the *down-pointing triangle* (🔽) behind
MAC-address, and I chose the MAC address that was presented. Below,
for 'Cloned Address', I chose 'Stable'. And this seemed to work.

I guess you can also configure this by editing the file
`/etc/NetworkManager/system-connections/`*ssid*. You should add
```
mac-address=(some mac-address)
cloned-mac-address=stable
```
to the `[wifi]`-section of that file. In that case, you may have
to restart the NetworkManager service.

Anyway, I'm glad that I could work around the problem. And I hope,
by publishing this on my blog, that I might help solving someone
else's problem as well.

**Update (2021-11-23):**

Today the problem magically re-appeared. So here is how I fixed it
this time:

```
sudo rmmod iwlmvm
sudo rmmod iwlwifi
sudo modprobe iwlwifi
```

