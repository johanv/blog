<!--
.. title: Mannen in het verkeer
.. slug: mannen-in-het-verkeer
.. date: 2025-02-25 13:06:00 UTC+01:00
.. tags: kansrekenen
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /galleries/cards/traffic.png
-->

Vanmorgen was in het nieuws dat
[8 op de 10 verkeersdoden in België mannen zijn](https://www.vrt.be/vrtnws/nl/2025/02/25/80-procent-van-de-verkeersdoden-zijn-mannen/).
Daarna volgde op radio 1 een hele polemiek over hoe mannen zich al dan niet overschatten
in het verkeer, en Gazet van Antwerpen begint een discussie over hoe
[mannen een groter gevaar in het verkeer zijn dan vrouwen](https://www.gva.be/binnenland/discussie.-de-cijfers-bevestigen-het-maar-vind-jij-mannen-ook-een-groter-gevaar-in-het-verkeer-dan-vrouwen/44309534.html),
want de cijfers bevestigen dat, nietwaar.

![verkeer op de straat](/galleries/cards/traffic.jpg)

Als zo'n discussies de ronde doen, is het altijd interessant om daar eens wat wiskunde op los te laten.

8 op 10 van de verkeersdoden is een man.
Voor het gemak ga ik dan ook even van de veronderstelling uit dat 8 op de 10 chauffeurs die een gevaar
op de weg zijn, mannen zijn. (Dat is misschien wat bij de haren getrokken, maar het lijkt me nog een redelijke
aanname voor het verhaal dat ik hier wil doen.)

Waar ik ook een gooi naar ga doen, is het percentage van de Belgische chauffeurs die gevaarlijk rijden.
Dat is moeilijk te zeggen, maar laat ons met de natte vinger veronderstellen: 
1 op 20, ofwel 5%. In België rijden ongeveer
6 miljoen personenwagens rond. Ik neem aan dat er dan ook 6 miljoen bestuurders zijn. Als we
veronderstellen dat 5% daarvan gevaarlijke chauffeurs zijn, dan zijn dat er ongeveer 300000. 
Aangezien er in België
[jaarlijks ongeveer 37000 verkeersongevallen gebeuren](https://statbel.fgov.be/nl/themas/mobiliteit/verkeer/verkeersongevallen),
is die 1/20 waarschijnlijk een overschatting; voel je vrij om mijn redenering opnieuw te maken
met een ander percentage gevaarlijke chauffeurs.

Ok, nu het punt dat ik wil maken. Hier is een stelling uit het kansrekenen, die quasi direct voortvloeit uit
de definitie van voorwaardelijke kansen:

```
P(A|B) = P(B|A) x P(A)/P(B)
```

De kans dat een gebeurtenis A zich voordoet, gegeven dat een gebeurtenis B zich voordoet, is gelijk
aan de kans dat gebeurtenis B zich voordoet, gegeven dat gebeurtenis A zich voordoet, vermenigvuldigd
met de kans op A gedeeld door de kans op B.

Vervang A door 'iemand is een gevaarlijke bestuurder' en B door 'iemand is een man'.

De kans dat een man een gevaarlijke bestuurder is, is gelijk aan de kans dat een gevaarlijke bestuurder
een man is (8/10), vermenigvuldigd met de kans op gevaarlijke bestuurders (1/20) gedeeld door de
kans op mannen (1/2).

```
P(A|B) = 0.8 x 0.05/0.5 = 0.08
```

Enfin, de kans dat een man een gevaarlijke bestuurder is, is dus 8%. Dat valt dus nog wel best
mee, hé.

Versta me niet verkeerd: als de meeste verkeersdoden mannen zijn, is het een goed idee om
mannen te sensibiliseren over hun gedrag in het verkeer. Want als je in die doelgroep het
aantal verkeersdoden naar beneden krijgt, dan weegt dat door in het totale aantal.

Maar begin nu niet met 'mannen zijn een gevaar op de weg', als die kans maar 8% is. Het levert
wel veel kliks en discussies op, maar verder heeft dat weinig toegevoegde waarde.
