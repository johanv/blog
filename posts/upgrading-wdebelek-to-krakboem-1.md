<!--
.. title: Upgrading Wdebelek to krakboem 1.0
.. slug: upgrading-wdebelek-to-krakboem-1
.. date: 2024-02-10 15:42:00 UTC+01:00
.. tags: php,eventsourcing,symfony,krakboem,wdebelek,wiezen,whist
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /galleries/cards/wdebelek.png
-->

As I mentioned in a previous blog post, I recently released
[version 1.0 of rva-vzw/krakboem](/posts/my-first-symfony-bundle), the custom
php library I created to use event sourcing and CQRS in my applications.
It comes together with a Symfony bundle,
[rva-vzw/krakboem-bundle](https://gitlab.com/rva-vzw/krakboem-bundle/), which
is now at version 0.1.2.

The first project to use version 1.0, was
[dikdikdik](/en/posts/wiezen-score-app), the score app for the 
[wiezen](https://en.wikipedia.org/wiki/Solo_whist) card game. This is
one of my _pet projects_, in fact the one that I regulary work on.
So in order to test if the krakboem-bundle and the updated krakboem
actually worked, I tried to use them for dikdikdik. As everything seemed
to work fine, I tagged it krakboem 1.0.

After that, I wanted to use krakboem 1.0 in [wdebelek](/en/posts/wiezen-app),
the app I wrote in covid-times, to play cards online.

![screenshot of the wdebelek card playing app](/galleries/cards/wdebelek.png)


Wdebelek also uses krakboem, but it is different in that it uses
event based entity repositories instead of
[deciders](/posts/whist-with-a-decider). So I had to figure out if this
would work out fine as well. And while doing he upgrade, I had a nice
chance to improve
[the upgrade instructions for krakboem 1.0](https://gitlab.com/rva-vzw/krakboem/-/blob/develop/UPGRADE-1.0.md).

I can't say the update was an easy one. Krakboem 1 requires php 8.3 and Symfony 
serializer 6. And for krakboem-bundle, I need Symfony 6.4.
All this means that I had to update some other dependencies as well. For php 8.3, it
mainly came down to updating
a package here and there. But since until last week, Wdebelek was built on Symfony 5.3, I had
quite some work with the Symfony upgrades. (**update 2024-02-14:** Especially the new
recipes for codeception 5 were not straightforward.)

The update of kraboem, from v 0.11 to 1.0, was also not trivial. Lots of classes
moved around. And quite some abstract base classes became read-only, a concept I wasn't
aware of back in 2020. (**update 2024-02-15:** Identifiers are now expected to be
non-empty as well. This was also quiet an update.)

But since last week, the 4th of february to be exact, wdebelek is running with
the latest versions of my libraries. It still needed a blog post, but hey,
here it is, so I am a happy programmer again. (Writing the blog post took me
a week, because of, you know, life.)

If you want to try playing cards with Wdebelek,
you have to find at least 2 other people to play, and go to
[kaart.rijkvanafdronk.be](https://kaart.rijkvanafdronk.be). Remember that you
need to drag the cards to play them. (This is not really clear from
the UI, and it often causes some confusion).

I still have one project to update: [1jg](https://gitlab.com/johanv/1jg),
another score app for another game. This will probably a little easier than
the Wdebelek update, because 1jg is already on Symfony 6.3. And of course
I now have nice upgrade instructions for krakboem and its bundle
now 😉.

