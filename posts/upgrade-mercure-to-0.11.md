<!--
.. title: Updating your Symfony app from Mercure 0.10 to Mercure 0.11
.. slug: upgrade-mercure-to-0.11
.. date: 2021-06-26 07:59:00 UTC+02:00
.. tags: mercure,symfony
.. category:
.. link:
.. description:
.. type: text
.. previewimage: /galleries/cards/mercure011.png
-->

Last wednesday I updated
[dikdikdik](https://gitlab.com/rva-vzw/dikdikdik/), a score sheet app
for [solo whist](https://en.wikipedia.org/wiki/Solo_whist),
so that it now uses [Mercure](https://mercure.rocks) v0.11, instead of v0.10.
I use docker-compose for the development environment of dikdikdik, and 
[GitLab CI/CD to run the end-to-end tests](/posts/gitlab-ci-selenium-and-mercure/)
with codeception and selenium.

![Mercure v0.11](/galleries/cards/mercure011.png)

This was not a trivial update, because v0.11 was a
[major milestone](https://dunglas.fr/2020/11/the-mercure-rocks-hub-is-now-based-on-caddy-web-server/)
for the Mercure project.
In retrospect, I didn't have to change much, but it took me some time
to find out what exactly I had to change, especially to make the 
end-to-end tests running again.

<!-- TEASER_END -->

Let's have a look at the changes I made in
[the merge request](https://gitlab.com/rva-vzw/dikdikdik/-/merge_requests/159/diffs).

First of all, my `config/packages/mercure.yaml` file, looks like this now:

```yaml
mercure:
    hubs:
        default:
            url: '%env(MERCURE_URL)%'
            public_url: '%env(MERCURE_PUBLIC_URL)%'
            jwt:
                secret: '%env(MERCURE_JWT_SECRET)%'
                publish: '*'
```

I should probably mention that I'm using version 5.3 of `symfony/mercure`,
and version 0.3.2 of `symfony/mercure-bundle`.

To be honest, I don't know exactly what this `mercure.yaml` file is about.
I guess it came as a recipe with `mercure-bundle`.
Anyway, I configured those variables
in `.env.dev` and `.env.test`:
```
MERCURE_URL=http://mercure:3000/.well-known/mercure
MERCURE_PUBLIC_URL=http://mercure:3000/.well-known/mercure
# The default token is signed with the secret key: !ChangeMe!
MERCURE_JWT_TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZXJjdXJlIjp7InB1Ymxpc2giOltdfX0.Oo0yg7y4yMa1vr_bziltxuTCqb8JVHKxp-f_FwwOim0
MERCURE_JWT_SECRET="!ChangeMe!"
```
(As you'd expect, I won't share the variables for my production
environment here. To be honest: I didn't roll out the update in production yet. If
I run into troubles with that, I'll keep you posted.)

You might also notice that I didn't use the standard port number; I kept using the same
port number as for mercure 0.10.

In `docker-compose.yml` I updated the definition for the mercure service
as follows:
```yaml
  mercure:
    image: dunglas/mercure:v0.11
    restart: unless-stopped
    environment:
      SERVER_NAME: ':3000'
      MERCURE_PUBLISHER_JWT_KEY: '!ChangeMe!'
      MERCURE_SUBSCRIBER_JWT_KEY: '!ChangeMe!'
    # Development mode:
    command: /usr/bin/caddy run -config /etc/caddy/Caddyfile.dev
    ports:
      - "3000:3000"
```

And last but not least, for `.gitlab-ci.yml`, I needed these variables for the
`dunglas/mercure` service:

```yaml
  MERCURE_PUBLISHER_JWT_KEY: '!ChangeMe!'
  MERCURE_SUBSCRIBER_JWT_KEY: '!ChangeMe!'
  SERVER_NAME: ':3000'
  MERCURE_EXTRA_DIRECTIVES: |
    cors_origins *
    publish_origins *
    demo
    anonymous
    subscriptions
```

If you wonder why I took these particular `MERCURE_EXTRA_DIRECTIVES`: I just looked
into the mercure container, and copied the directives that are in `Caddyfile.dev`,
but not in `Caddyfile`. I guess that should do for a dev-environment.
(If you want to try that as well; I did it like this:)

```bash
docker-compose exec mercure cat /etc/caddy/Caddyfile
docker-compose exec mercure cat /etc/caddy/Caddyfile.dev
```

That made it work for me. I hope it will work for you as well. As always:
if I did something wrong here, please let me know!

