<!--
.. title: Dependency hell when upgrading symfony from 4.3 to 4.4
.. slug: symfony-4.4-upgrade-issues
.. date: 2020-04-10 21:19:00 UTC+02:00
.. tags: symfony,php
.. category:
.. link:
.. description:
.. type: text
-->

Recently I tried to upgrade the symfony version of the
[dikdikdik wiezen score app](https://gitlab.com/rva-vzw/dikdikdik)
from 4.3 to 4.4. As usual, I ended up in dependency hell.

Here's what I got:

```
$ composer update symfony/*
Loading composer repositories with package information
Updating dependencies (including require-dev)
Restricting packages listed in "symfony/symfony" to "4.4.*"
Your requirements could not be resolved to an installable set of packages.

  Problem 1
    - Conclusion: don't install symfony/validator v4.4.7
    - Conclusion: don't install symfony/validator v4.4.6
    - Conclusion: don't install symfony/validator v4.4.5
    - Conclusion: don't install symfony/validator v4.4.4
    - Conclusion: don't install symfony/validator v4.4.3
    - Conclusion: don't install symfony/validator v4.4.2
    - Conclusion: don't install symfony/validator v4.4.1
    - symfony/validator v4.4.0 conflicts with doctrine/lexer[v1.0.1].
    - symfony/validator v4.4.0 conflicts with doctrine/lexer[v1.0.1].
    - symfony/validator v4.4.0 conflicts with doctrine/lexer[v1.0.1].
    - Installation request for symfony/validator 4.4.* -> satisfiable by symfony/validator[v4.4.0, v4.4.1, v4.4.2, v4.4.3, v4.4.4, v4.4.5, v4.4.6, v4.4.7].
    - Installation request for doctrine/lexer (locked at v1.0.1) -> satisfiable by doctrine/lexer[v1.0.1].
```

I ran `composer require doctrine/lexer`, hoping that this would give
me some clues:

```
$ composer depends doctrine/lexer
doctrine/annotations  v1.8.0   requires  doctrine/lexer (1.*)
doctrine/common       v2.10.0  requires  doctrine/lexer (^1.0)
```

I guess that doctrine must have been the problem.

So I uninstalled doctrine (`composer remove symfony/orm-pack`), 
[upgraded symfony](https://symfony.com/doc/current/setup/upgrade_minor.html) 
(that worked, 🎉),
put the doctrine configuration files back in place, and reinstalled
doctrine (`composer require symfony/orm-pack`).

It did update something, but then I got this error:

```
Executing script cache:clear [KO]
 [KO]
Script cache:clear returned with error code 255
!!  PHP Fatal error:  Uncaught Symfony\Component\Debug\Exception\ClassNotFoundException: Attempted to load class "DoctrineCacheBundle" from namespace "Doctrine\Bundle\DoctrineCacheBundle".
!!  Did you forget a "use" statement for another namespace?
```

After [some searching on the internet](https://stackoverflow.com/a/59497868/1417449),
I found out that I had to remove the line
```
Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle::class => ['all' => true],
```
[from `app.php`](https://gitlab.com/rva-vzw/dikdikdik/-/merge_requests/132/diffs?commit_id=90e133ac9c2a4766f155ca1881984fa66500444f)
to fix that one.

Of course now I had various other issues, but the dependency hell was fixed,
so I am glad I can share this with you.
