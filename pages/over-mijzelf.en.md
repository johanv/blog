<!--
.. title: About me
.. slug: over-mijzelf
.. date: 2022-06-17 10:20:00 UTC+02:00
.. tags: johanv
.. link:
.. description:
.. type: text
-->

## Programmer

I am a programmer. So I write code, at this time mainly for web applications. And since I've been doing this for more than 20 years, I kinda know the things I'm good at, and the things I'm not that good at.

Refactoring is one of the things I'm good at. I can take existing code spaghetti, and isolate independent testable services. I also like to introduce interfaces, which clarifies the dependencies between services, and makes them easier to unit test.

I write a lot of tests. Unit tests for new code, web tests for existing code that might be difficult to understand, but that needs to be working after I fiddled with it. I want my tests to run automatically on every new merge request.

CQRS and event buses are also things I like to use. For some time, I've been working
on an event sourced 
[score app for the wiezen (solo whist) card game](https://www.rijkvanafdronk.be/app).
DDD is something that I find very interesting, but I still have a lot to learn about that.

## Misc

I like Linux and open source, and I like Belgian beers. I play cards 
([wiezen](https://www.rijkvanafdronk.be)), 
but I didn't manage to become the world champion (yet). And I will probably stay a little 'Johan of the Chiro' forever. My head is always full of ambitous plans, about things I want to develop, or things I want to blog about. 

## Disclaimer

To be clear: I don't know anything about Word and Excel. I know about printers that they often don't work. And I can't work on a computer if it lacks decent command line tools.

