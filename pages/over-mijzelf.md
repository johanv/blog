<!--
.. title: Over mijzelf
.. slug: over-mijzelf
.. date: 2022-06-17 10:20:00 UTC+02:00
.. tags: johanv
.. link:
.. description:
.. type: text
-->

## Programmeur

Ik ben programmeur. Ik programmeer, op  dit moment voornamelijk webapplicaties. En aangezien ik dat al meer dan 20  jaar doe, heb ik een vrij goed zicht op waar ik goed in ben, en waar niet.

Ik kan erg goed refactoren: Uit bestaande codespaghetti op zichzelf staande testable services isoleren. En interfaces introduceren, zodat testen makkelijker wordt, en dependencies duidelijker.

Ik schrijf ook veel tests. Unit tests voor nieuwe code, web tests voor onoverzichtelijke bestaande code, die vooral moet blijven werken nadat ik eraan gesleuteld heb. Ik heb het liefst dat mijn tests automatisch lopen op ieder nieuw merge request. 

CQRS en een event bus zijn ook dingen die ik graag gebruik. Ik werk al een tijdje
aan een event sourced 
[scoretoepassing voor het wiezen](https://www.rijkvanafdronk.be/app), een fijn
maar tijdsintensief hobbyprojectje. DDD vind ik erg interessant, al valt er voor mij
op dat vlak nog wel wat te leren.


## En verder

Ik ben fan van Linux en open source, en ook van Belgisch bier. Ik speel kaart 
([wiezen](https://www.rijkvanafdronk.be)); ik ambieer nog steeds wereldkampioen
wiezen te worden.
En ik zal altijd een beetje 'Johan van de Chiro' blijven.

## Disclaimer

O ja, voor de duidelijkheid: Ik ken niets van Word en Excel. Van printers weet ik alleen dat ze vaak niet werken. En ik kan niet werken op een systeem zonder deftige command line tools.

