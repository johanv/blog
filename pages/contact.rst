.. title: Contact info
.. slug: contact
.. date: 2018-08-29 17:08:13 UTC+2
.. tags: johan
.. link:
.. description:
.. type: text

You can contact me on:

-  **mastodon:** https://phpc.social/@vohanj
-  **bluesky:** https://bsky.app/profile/johanv.org
-  **gitlab:** https://gitlab.com/johanv
-  **github:** https://github.com/johanv
-  **untappd:** https://untappd.com/user/johanv
-  **facebook:** http://www.facebook.com/johan.vervloet
-  **keybase:** https://keybase.io/johanv

My e-mail address is joohanv at johanv dot org.
(I deliberately put an error in there, to avoid spam. I guess you'll
be able to figure it out.) You may use my PGP public key
to encrypt your messages. You can find it on my keybase profile.
